#include <u.h>
#include <libc.h>

void 
main(int argc, char *argv[])
{
	int fp, nf, c, r;
	char buf[1];
	long n;

	/* input handler */
	if(argc == 1){
		print("no arg supplied\n");
		exits("no arg supplied");
	} else if((fp = open(argv[1], OREAD)) < 0){
		perror(argv[1]);
		exits(0);
	}

	/* first pass, get num of delims */
	while((n=read(fp, buf, 1))>0){
		if((buf[0] - '\0') == '\n'){
			nf = 1;
			/* continue saves cycles */
			continue;
		}

		if((buf[0] - '\0') == '%' && nf == 1){
			nf = 0;
			c++;
		}
	}
	
	/* reset */
	seek(fp, 0, 0);
	r = truerand() % ((c-0) + 0);
	c = 0;

	/* second pass, printing */
	while((n=read(fp, buf, 1))>0){
		if((buf[0] - '\0') == '\n'){
			nf = 1;
			/* no continue here, we want to print newlines */
		}

		if((buf[0] - '\0') == '%' && nf == 1){
			nf = 0;
			c++;
			/* continue here, don't want to print % */
			continue;
		}
	
		if(c == r)
			print("%c", buf[0]);

		if(c > r){
			close(fp);
			print("\n");
			exits(0);
		}
	}

	if(n < 0)
		perror(argv[1]);
	
	/* just in case */
	close(fp);
}
