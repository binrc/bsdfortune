build:
	6c main.c && 6l main.6 && mv 6.out bsdfortune && rm main.6

run: build
	./bsdfortune files/ascii-art

debug: build
	acid ./6.out

install: build
	cp bsdfortune /bin/bsdfortune

installfiles:
	mkdir /lib/fortune && cp ./files/* /lib/fortune/

plan9port:
	9c main.c && 9l main.o && mv a.out bsdfortune && rm main.o
	
